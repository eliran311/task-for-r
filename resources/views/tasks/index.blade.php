@extends('layouts.app')
@section('content')
<H1>All tasks</H1>
@if (Request::is('tasks'))
<a href="{{route('myfilter')}}">My Tasks</a>
@else
<a href="{{action('TaskController@index')}}">All Tasks</a>

 @endif
    
    


<table >
<thead><tr>
<th > id</th>
<th > NameOfTask</th>


</tr> </thead>
<tbody>@foreach($tasks as $task) 
<div class = "try"><tr >
<td> {{$task->id}}</td>  
<td> {{$task->title}}</td>  
 <td><a href="{{route('tasks.edit', $task->id)}}">Edit</a></td> 
 @can('manager')<td><a href="{{route('delete', $task->id)}}">Delete</a></td>  @endcan
 @if ($task->status==0)
 @can('manager')<td><a href="{{route('done', $task->id)}}">mark as done</a></td>@endcan
@else
<td><label>done!</label></td>
@endif
</tr></div>

@endforeach
</tbody>

<a  href="{{route('tasks.create')}}">Create a new task</a>


@endsection


<style>
table,th,td{
     border: 1px solid black;
     height: 10vh;
      margin: 0;
      
}
.el{
   
    margin-bottom: 10px;
}
.try{
    padding: 0 25px ! important;
    font-size: 50px ! important;
}
th,td{
    padding: 0 25px ! important;
}

</style>
