@extends('layouts.app')
@section('content')

<h1>Create a new task</h1>
<form method = 'post' action = "{{action('TaskController@store')}}" >
{{csrf_field()}}      
    

<div class = "form-group">
    <label for = "title">Task name </label>
    <input type = "text" class = "form-control" name = "title">
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach

</div>

<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Save">
</div>

</form>

@endsection



