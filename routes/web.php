<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::resource('tasks','TaskController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('tasks/delete/{id}', 'TaskController@destroy')->name('delete');
Route::get('tasks/done/{id}', 'TaskController@done')->name('done');
Route::get('filter/', 'TaskController@mytasks')->name('myfilter');